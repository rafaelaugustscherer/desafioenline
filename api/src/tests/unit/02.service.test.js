const chai = require('chai');
const Sinon = require('sinon');

const fileMocks = require('./mocks/file');
const fileService = require('../../app/service/file');
const fileModel = require('../../app/model/file');

const { expect } = chai;
const { assert } = Sinon;

describe('Teste do File Service', () => {

  afterEach(() => {
    Sinon.restore();
  });

  describe('Teste do método uploadMetadata', () => {
    
    it('Chama o model e não retorna nenhum dado', async () => {
      const modelStub = Sinon.stub(fileModel, 'insertMany').resolves([fileMocks.mongoDbFileMock]);
      const { _id, ...expectedCall } = fileMocks.mongoDbFileMock;
      
      const response = await fileService.uploadMetadata(fileMocks.fileObjectMock);
      
      assert.calledOnceWithExactly(modelStub, expectedCall);
      expect(response).to.be.undefined;
    });

    it('Não retorna erro mesmo que o upload falhe', async () => {
      Sinon.stub(fileModel, 'insertMany').throws('Connection failed');
      try {
        const response = await fileService.uploadMetadata(fileMocks.fileObjectMock);
        expect(response).to.be.undefined;
      } catch (e) {
        expect(e).to.be.undefined;
      }
    });
  });
});