const fsFileList = [
  'file1.txt',
  'file2.js',
];

const fsFileStat = [
  { size: 184 },
  { size: 100 },
];

const filesWithMetadata = [
  { name: 'file1.txt', size: 184 },
  { name: 'file2.js', size: 100 },
];

const fileObjectMock = {
  originalname: 'file1.txt',
  mimetype: 'text/plain',
  size: 184,
  path: 'src/uploads/aaa-1234/file1.txt',
};

const mongoDbFileMock = {
  _id: 'aaa032random100',
  name: 'file1.txt',
  type: 'text/plain',
  size: 184,
  path: 'src/uploads/aaa-1234/file1.txt',
};

module.exports = {
  fsFileList,
  fsFileStat,
  filesWithMetadata,
  fileObjectMock,
  mongoDbFileMock,
};