const chai = require('chai');
const Sinon = require('sinon');
const fs = require('fs');

const fileMocks = require('./mocks/file');
const fileController = require('../../app/controller/file');
const fileService = require('../../app/service/file');

const { expect } = chai;
const { assert, spy } = Sinon;

describe('Teste do File Controller', () => {
  let resSpy;
  
  beforeEach(() => {
    resSpy = spy({
      status: () => resSpy,
      json: () => resSpy,
      download: () => resSpy,
    });
  });

  afterEach(() => {
    Sinon.restore();
  });

  describe('Teste do método getFolderRef', () => {
    let firstFolderRef;

    it('Retorna status 200 com a propriedade \'ref\' na resposta', async () => {
      await fileController.getFolderRef({}, resSpy);
      const folderRef = resSpy.json.args[0];
      
      assert.calledOnceWithExactly(resSpy.status, 200);
      assert.calledOnce(resSpy.json);
      expect(folderRef).to.not.be.undefined;
      expect(folderRef[0]).to.have.property('ref');

      firstFolderRef = folderRef[0].ref;
    });

    it('Na segunda chamada a \'ref\' é diferente da primeira', async () => {
      await fileController.getFolderRef({}, resSpy);
      const folderRef = resSpy.json.args[0];

      expect(folderRef).to.not.be.undefined;
      expect(folderRef[0]).to.have.property('ref');
      expect(folderRef.ref).not.to.be.equal(firstFolderRef);
    });
  });

  describe('Teste do método getFiles', () => {
    const reqMock = { params: '1234-a' };
    
    it('Retorna status 200 com os dados dos arquivos', async () => {
      Sinon.stub(fs, 'readdirSync').returns(fileMocks.fsFileList);
      Sinon.stub(fs, 'statSync')
        .onFirstCall().returns(fileMocks.fsFileStat[0])
        .onSecondCall().returns(fileMocks.fsFileStat[1]);

      await fileController.getFiles(reqMock, resSpy);

      assert.calledOnceWithExactly(resSpy.status, 200);
      assert.calledOnce(resSpy.json);
      expect(resSpy.json.args[0][0]).to.be.deep.equal(fileMocks.filesWithMetadata);
    });

    it('Retorna array vazio de dados em caso de arquivos inexistentes', async () => {
      Sinon.stub(fs, 'readdirSync').throws('Folder does not exist');

      await fileController.getFiles(reqMock, resSpy);

      assert.calledOnceWithExactly(resSpy.status, 200);
      assert.calledOnce(resSpy.json);
      expect(resSpy.json.args[0][0]).to.be.deep.equal([]);
    });
  });

  describe('Teste do método uploadFile', () => {
    
    it('Retorna status 201 com uma mensagem de sucesso', async () => {
      const reqMock = { file: () => {} };
      const serviceStub = Sinon.stub(fileService, 'uploadMetadata').resolves();

      await fileController.uploadFile(reqMock, resSpy);

      assert.calledOnceWithExactly(serviceStub, reqMock.file);
      assert.calledOnceWithExactly(resSpy.status, 201);
      assert.calledOnce(resSpy.json);
      expect(resSpy.json.args[0][0]).to.have.property('message');
    });

    it('Retorna status 401 e um erro em caso de arquivo não enviado', async () => {
      const reqMock = {};
      const serviceStub = Sinon.stub(fileService, 'uploadMetadata').throws();

      try {
        await fileController.uploadFile(reqMock, resSpy);
      } catch (e) {
        expect(e).to.be.undefined;
      }

      assert.notCalled(serviceStub);
      assert.calledOnceWithExactly(resSpy.status, 401);
      assert.calledOnce(resSpy.json);
      expect(resSpy.json.args[0][0]).to.have.property('error');
    });
  });

  describe('Teste do método downloadFile', () => {
    const reqMock = { params: {
      folderRef: '1234-a',
      filename: 'file1.txt',
    } };
    
    it('Retorna status 200 e faz o download do arquivo', async () => {
      const filePath = `src/uploads/${reqMock.params.folderRef}/${reqMock.params.filename}`;
      Sinon.stub(fs, 'readFileSync').returns(filePath);

      await fileController.downloadFile(reqMock, resSpy);

      assert.calledOnceWithExactly(resSpy.download, filePath);
    });

    it('Retorna status 404 e um erro em caso de arquivo não encontrado', async () => {
      Sinon.stub(fs, 'readFileSync').throws('File not found');

      await fileController.downloadFile(reqMock, resSpy);

      assert.calledOnceWithExactly(resSpy.status, 404);
      assert.calledOnce(resSpy.json);
      expect(resSpy.json.args[0][0]).to.have.property('error');
    });
  });
});