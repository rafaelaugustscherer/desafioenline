const { model, Schema } = require('mongoose');

const fileSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  type: {
    type: String,
  },
  size: {
    type: Number,
    required: true,
  },
  path: {
    type: String,
    required: true,
  },
}, {
  versionKey: false,
});

module.exports = model('files', fileSchema);