const { connect } = require('mongoose');

const { MONGO_USER, MONGO_PASSWORD } = process.env;
const uri = 'mongodb+srv://cluster0.gfw2a.mongodb.net/?retryWrites=true&w=majority';
const options = {
  user: MONGO_USER,
  pass: MONGO_PASSWORD,
  autoIndex: true,
  dbName: 'desafio-enline',
};

const connectToDatabase = () => connect(uri, options, (err) => {
  if (err) {
    console.info(
      `ℹ️ Could not connect to MongoDB: ${err.message}`,
    );
  }
  else {
    console.log('ℹ️ Successfully connected to MongoDB');
  }
});

module.exports = connectToDatabase;
