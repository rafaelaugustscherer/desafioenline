const fileModel = require('../model/file');

const uploadMetadata = async (file) => {
  const {
    originalname: name,
    mimetype: type,
    size,
    path,
  } = file;

  try {
    await fileModel.insertMany({ name, type, size, path });
  } catch (e) {
    console.info('ℹ️ Could not connect to MongoDB to send file metadata');
  }
};

module.exports = {
  uploadMetadata,
};