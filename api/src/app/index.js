require('dotenv/config');
const express  = require('express');
const cors = require('cors');

const appRouter = require('./router');
const connectToDatabase = require('./model/connection');

const app = express();
app.use(cors());
app.use(appRouter);

connectToDatabase();

const { PORT } = process.env;
const port = PORT || 3001;

app.listen(PORT || 3001, () => (
  console.log(`ℹ️ App listening at ${port}`)
));