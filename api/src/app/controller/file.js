const fs = require('fs');
const uuid = require('uuid');

const fileService = require('../service/file');

const getFolderRef = async (_req, res) => {
  const folderRef = uuid.v4();

  return res.status(200).json({
    ref: folderRef,
  });
};

const getFiles = async (req, res) => {
  try {
    const { folderRef } = req.params;
    const folderPath = `src/uploads/${folderRef}`;

    const folderFiles = fs.readdirSync(folderPath);

    const filesMetadata = folderFiles.map((file) => {
      const fileSize = fs.statSync(`${folderPath}/${file}`).size;
      return { name: file, size: fileSize };
    });

    return res.status(200).json(filesMetadata);
  } catch (e) {
    return res.status(200).json([]);
  }
};

const uploadFile = async (req, res) => {
  if (!req.file) {
    return res.status(401).json({
      error: 'No file was sent',
    });
  }
  fileService.uploadMetadata(req.file);

  return res.status(201).json({
    message: 'File upload successfully',
  });
};

const downloadFile = async (req, res) => {
  try {
    const { folderRef, filename } = req.params;
    const filePath = `src/uploads/${folderRef}/${filename}`;
    fs.readFileSync(filePath);

    return res.download(filePath);
  } catch (e) {
    return res.status(404).json({
      error: 'File not found',
    });
  }
};

module.exports = {
  getFolderRef,
  getFiles,
  uploadFile,
  downloadFile,
};