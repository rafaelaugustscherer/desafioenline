const { Router } = require('express');
const FileController = require('../controller/file');
const MulterRouter = require('./multer');

const fileRouter = Router();

fileRouter.route('/file/ref')
.get(FileController.getFolderRef);

fileRouter.route('/file/:folderRef')
  .get(FileController.getFiles);

fileRouter.route('/file/download/:folderRef/:filename')
  .get(FileController.downloadFile);

fileRouter.route('/file/upload/:folderRef')
  .post(MulterRouter.single('newFile'), FileController.uploadFile);
 
module.exports = fileRouter;