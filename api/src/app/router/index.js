const { Router } = require('express');

const fileRouter = require('./file');

const appRouter = Router();

appRouter.use(fileRouter);

module.exports = appRouter;