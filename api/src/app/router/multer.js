const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, _res, next) => {
    const { folderRef } = req.params;
    const folderPath = `src/uploads/${folderRef}`;
    
    fs.mkdirSync(folderPath, { recursive: true });
    next(null, folderPath);
  },

  filename: (_req, file, next) => {
    next(null, file.originalname);
  },
});

const upload = multer({ storage });

module.exports = upload;