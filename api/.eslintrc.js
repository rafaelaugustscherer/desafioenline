module.exports = {
    'env': {
        'browser': true,
        'commonjs': true,
        'es2021': true,
    },
    'parserOptions': {
        'ecmaVersion': 'latest',
    },
    'rules': {
        'quotes': ['error', 'single'],
        'comma-dangle': ['error', 'always-multiline'],
        'semi': ['error', 'always'],
        'no-unused-vars': ['error', {
            ignoreRestSiblings: true,
            argsIgnorePattern: '^_',
        }],
    },
};
