import { createContext, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import fileUtilities from '../utilities/file';

export const FileContext = createContext();

const FileProvider = ({ children }) => {
  const [folderRef, setFolderRef] = useState();
  const [ files, setFiles ] = useState([]);

  const fetchFolderRef = async () => {
    const ref = await fileUtilities.getFileRef();
    setFolderRef(ref);
  };

  const fetchFiles = async () => {
    const files = await fileUtilities.getFiles(folderRef);
    setFiles(files);
  };

  const uploadFile = async (file) => {
    const response = await fileUtilities.uploadFile(file, folderRef);

    if (response.error) {
      return response;
    }

    const filenameExists = files.find(({ name }) => name === file.name);
    if (!filenameExists) {
      setFiles([ ...files, file ]);
    }

    return { message: 'Arquivo enviado com sucesso!' };
  };

  const downloadFile = async (fileName) => {
    fileUtilities.downloadFile(fileName, folderRef);
  };

  useEffect(() => {
    fetchFolderRef();
  }, []);

  useEffect(() => {
    if (folderRef) fetchFiles();
  }, [folderRef]);

  const value = {
    files,
    uploadFile,
    downloadFile,
  };

  return (
    <FileContext.Provider value={value}>
      {children}
    </FileContext.Provider>
  );
};

FileProvider.propTypes = {
  children: PropTypes.arrayOf(PropTypes.element),
};

export default FileProvider;