import download from 'downloadjs';

const getFileRef = async () => {
  const { REACT_APP_SERVER } = process.env;
  const currentRef = localStorage.getItem('enlineFolderRef');

  if (currentRef && typeof currentRef === 'string') {
    return currentRef;
  }

  const response = await fetch(`${REACT_APP_SERVER}/file/ref`)
    .then((res) => res.json());

  localStorage.setItem('enlineFolderRef', response.ref);
  return response.ref || null;
};

const getFiles = async (ref) => {
  const { REACT_APP_SERVER } = process.env;

  const response = await fetch(`${REACT_APP_SERVER}/file/${ref}`)
    .then((res) => res.json());

  return response || [];
};

const uploadFile = async (file, ref) => {
  const { REACT_APP_SERVER } = process.env;
  const formData = new FormData();
  formData.append('newFile', file);

  const response = await fetch(`${REACT_APP_SERVER}/file/upload/${ref}`, {
    method: 'POST',
    body: formData,
  }).then((res) => {
    if (res.status === 201) {
      return 'OK';
    }
    if (res.body.error) {
      return res.body;
    }
    return { error: 'Could not POST file' };
  });

  return response;
};

const downloadFile = async (file, ref) => {
  const { REACT_APP_SERVER } = process.env;

  const response = await fetch(`${REACT_APP_SERVER}/file/download/${ref}/${file}`);
  const blob = await response.blob();
  download(blob, file);
};

export default {
  getFileRef,
  getFiles,
  uploadFile,
  downloadFile,
};

