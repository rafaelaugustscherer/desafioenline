import { useContext, useState } from 'react';
import style from './styles/FileForm.module.css';
import { FileContext } from '../providers/FileProvider';

const FileForm = () => {
  const { uploadFile } = useContext(FileContext);
  const [popover, setPopover] = useState(({
    message: undefined,
    isError: false,
  }));

  const handlePopoverTimeout = () => (
    setTimeout(() => {
      setPopover({ message: undefined });
    }, 5000)
  );

  const handleFileSubmit = async (e) => {
    e.preventDefault();

    const input = e.target[0];
    const response = await uploadFile(input.files[0]);

    if (response.error) {
      setPopover({ message: response.error, isError: true });
    } else {
      setPopover({ message: response.message, isError: false });
    }
    handlePopoverTimeout();
  };

  return (
    <>
    { popover.message && (
      <div
        className={`
          ${style.popOverMessage}
          ${popover.isError ? style.popOverError : style.popOverSuccess}
        `}
      >
          { popover.message }
      </div>
    ) }
    <form className={style.form} onSubmit={handleFileSubmit}>
      <h2 className={style.formTitle}>Fazer upload de arquivo</h2>
      <input
        className={style.fileInput}
        type="file"
        name="file"
        data-testid="fileInput"
        required
      />
      <button
        className={style.submitFileBtn}
        type="submit"
        data-testid="uploadButton"
      >
        Fazer upload
      </button>
    </form>
    </>
  );
};

export default FileForm;