import React, { useContext } from 'react';
import { IoMdDownload } from 'react-icons/io';
import style from './styles/Files.module.css';
import { FileContext } from '../providers/FileProvider';
import DismissableAlert from './DismissableAlert';

const Files = () => {
  const { files, downloadFile } = useContext(FileContext);

  const bytesToKbString = (bytes) => {
    return `${(+bytes / 1024).toFixed(2)} Kb`;
  };

  const handleDownload = (filename) => {
    downloadFile(filename);
  };

  const warningText = 'Nota: Arquivos enviados com o mesmo nome substituirão os atuais';

  return (
    <>
      <DismissableAlert text={warningText} type='warning' />
      <table className={style.fileList} data-testid="fileTable">
        <thead>
          <tr>
            <th>Nome</th>
            <th>Tamanho</th>
            <th>Opções</th>
          </tr>
        </thead>
        <tbody>
          {
            files.length
              ? files.map((file, idx) => {
                return (
                  <tr key={`file-${idx}`}>
                    <td>{file.name}</td>
                    <td>{bytesToKbString(file.size)}</td>
                    <td>
                      <button
                        type="button"
                        className={style.downloadBtn}
                        onClick={() => handleDownload(file.name)}
                      >
                        Download <IoMdDownload />
                      </button>
                    </td>
                  </tr>
                );
              })
              : (
                <tr>
                  <td>Nenhum arquivo armazenado</td>
                  <td></td>
                  <td></td>
                </tr>
              )
          }
        </tbody>
      </table>
    </>
  );
};

export default Files;