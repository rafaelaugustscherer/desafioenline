import React, { useState } from 'react';
import { BsXLg } from 'react-icons/bs';
import PropTypes from 'prop-types';
import style from './styles/DismissableAlert.module.css';

const DismissableAlert = ({ text, type }) => {
  const [hidden, setHidden] = useState(false);
  
  const dismissAlert = () => {
    setHidden(true);
  };
  
  return (
    <div className={`
      ${style.alertWrapper}
      ${style[type]}
      ${hidden && style.displayNone}
    `}>
      <p>{ text }</p>
      <BsXLg
        className={style.dismissButton}
        onClick={dismissAlert}
      />
    </div>
  );
};

DismissableAlert.propTypes = {
  text: PropTypes.string,
  type: PropTypes.string,
};

export default DismissableAlert;