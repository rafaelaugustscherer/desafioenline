import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import App from '../App';

describe('Testa a tabela de arquivos', () => {
  beforeEach(() => {
    render(<App />);
  });
  
  test('Renderiza a tabela na tela', () => {
    const table = screen.getByTestId('fileTable');
    expect(table).toBeInTheDocument();
  });

  test('A tabela tem cabeçalhos \'Nome\', \'Tamanho\' e \'Opções\'', () => {
    const table = screen.getByTestId('fileTable');
    const headers = table.getElementsByTagName('th');

    expect(headers.length).toBe(3);
    expect(headers[0]).toHaveTextContent('Nome');
    expect(headers[1]).toHaveTextContent('Tamanho');
    expect(headers[2]).toHaveTextContent('Opções');
  });
});
