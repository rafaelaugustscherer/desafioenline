import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import App from '../App';

describe('Testa elemento de upload de arquivo', () => {
  beforeEach(() => {
    render(<App />);
  });

  test('Renderiza o componente para selecionar arquivos', () => {
    const uploadInput = screen.getByTestId('fileInput');
    expect(uploadInput).toBeInTheDocument();
  });

  test('Renderiza o botão para fazer o upload', () => {
    const uploadButton = screen.getByTestId('uploadButton');
    expect(uploadButton).toBeInTheDocument();
  });
});
