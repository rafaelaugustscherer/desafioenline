import React from 'react';
import './App.css';
import FileForm from './components/FileForm';
import Files from './components/Files';
import FileProvider from './providers/FileProvider';

function App() {
  return (
    <>
      <h1>Desafio Enline</h1>
      <FileProvider>
        <FileForm />
        <Files />
      </FileProvider>
    </>
  );
}

export default App;
