# DesafioEnline

Desafio Técnico Enline:

> O desafio consiste em uma Single Page Application [MERN](https://www.mongodb.com/mern-stack) para upload, visualização e download de arquivos.

---
## Deploy no Heroku

Deploy da aplicação é feito a partir do GitLab CI em forma de container para o [Heroku](https://www.heroku.com/).

O back-end está disponibilizado em um servidor Node.js a partir do tradicional `npm start`, enquanto o front-end está disponibilizado de forma estática através de um servidor NGINX.

* Front-end disponível em https://desafio-enline.herokuapp.com/
* Back-end disponível em https://desafio-enlineapi.herokuapp.com/

---
## Como rodar o projeto

<details>
  <summary>📦Rodar localmente com NPM 📦</summary>

  ## Requisitos

  - [Node.js](https://nodejs.org/)
  - npm (Node Package Manager)

  ## Setup

  Antes de inicializar o projeto, é importante configurar algumas variáveis de ambiente e instalar as dependências do projeto.

  ### Configurar o ambiente (.env)

  * Back-end (Opcional | Conexão com MongoDB)
    - Acesse o diretório `./api`

    - Altere o arquivo `.env.example` com as suas variáveis de ambiente
      ```
        PORT=3001 // Opcional - Porta na qual a aplicação vai rodar
        MONGO_USER=YOUR_MONGO_DB_USER // Opcional - Usuário para conexão com o banco MongoDB
        MONGO_PASSWORD=YOUR_MONGO_PASSWORD // Opcional - Senha para conexão com o banco MongoDB
      ```
    - Renomeie o arquivo para `.env`
  
  * Front-end
    - Acesse o diretório `./client`
    - Altere o arquivo `.env.example` com as suas variáveis de ambiente
      ```
        REACT_APP_SERVER='http://localhost:3001' // Servidor no qual a api está rodando
      ```
    - Renomeie o arquivo para `.env`
  
  ### Instalar dependências
  
  * Rode o comando `npm run install:apps` na raiz do projeto

  ## Inicializar a aplicação

  Inicialize o back-end e o front-end em **terminais separados**

  > Por padrão o back-end inicializa na porta 3001

  > Por padrão o front-end inicializa na porta 3000

  * Back-end:
    - Acesse o diretório `./api`
    - Rode o comando `npm run dev`

  * Front-end:
    - Acesse o diretório `./client`
    - Rode o comando `npm start`

  ## Acessar a aplicação

  * Back-end:
    - Você pode testar a aplicação via [Postman](https://www.postman.com/) ou [Insomnia](https://insomnia.rest/)
    - Rotas para o Postman estão disponibilizadas [aqui!](/api/desafioEnline.postman_collection.json)

  * Front-end:
    - Abra o seu navegador e insira a URL: `localhost:3000`
</details>

<details>
  <summary>🐋 Rodar localmente com o Docker 🐋</summary>

  ## Requisitos

  - [Docker](https://www.docker.com/get-started/)

  ## Setup

  Antes de inicializar o projeto, é importante configurar algumas variáveis de ambiente.

  ### Configurar o ambiente (.env)

  * Back-end (Opcional | Conexão com MongoDB)
    - Acesse o diretório `./api`

    - Altere o arquivo `.env.example` com as suas variáveis de ambiente
      ```
        PORT=3001 // Opcional - Porta na qual a aplicação vai rodar
        MONGO_USER=YOUR_MONGO_DB_USER // Opcional - Usuário para conexão com o banco MongoDB
        MONGO_PASSWORD=YOUR_MONGO_PASSWORD // Opcional - Senha para conexão com o banco MongoDB
      ```
    - Renomeie o arquivo para `.env`

  ## Inicializar a aplicação

  > Por padrão o back-end inicializa na porta 3001

  > Por padrão o front-end inicializa na porta 3000

  - Rode o comando `docker-compose up --build` na raiz da aplicação e aguarde a inicialização dos containers.

  ## Acessar a aplicação

  * Back-end:
    - Você pode testar a aplicação via [Postman](https://www.postman.com/) ou [Insomnia](https://insomnia.rest/)
    - Rotas para o Postman estão disponibilizadas [aqui!](/api/desafioEnline.postman_collection.json)

  * Front-end:
    - Abra o seu navegador e insira a URL: `localhost:3000`
</details>

---
## Testes

### Back-end

O back-end é testado com o uso das bibliotecas `mocha`, `chai` e `sinon` de forma unitária.

  * Para testar a aplicação entre no diretório `./api` e rode o compando `npm test`

### Front-end

O front-end é testado com o uso das bibliotecas `@testing-library/jest-dom`, `@testing-library/react`, `@testing-library/user-event` sendo todos eles testes de integração.

  * Para testar a aplicação entre no diretório `./client` e rode o compando `npm test`

## Tecnologias Utilizadas

* Back-end
  - <img src="https://cdn.iconscout.com/icon/free/png-256/node-js-1174925.png" alt="Node.js Logo" width="15"/> Node.js
    - Express
    - Multer
    - Mongoose
    - Cors
    - uuid
  - <img src="https://www.docker.com/favicon.ico" alt="Docker Logo" width="15"/> Docker

* Front-end
  - <img src="https://reactjs.org/favicon.ico" alt="React Logo" width="15"/> React.js
    - react-icons
    - downloadjs
  - <img src="https://axios-http.com/assets/favicon.ico" alt="Axios Logo" width="15"/> Axios
  - <img src="https://www.docker.com/favicon.ico" alt="Docker Logo" width="15"/> Docker


## Referências a outros projetos

Neste projeto foram utilizados alguns recursos e sintaxe de código inspirados em outros projetos que já havia realizado:

- 🏅 [TrybeRank](https://github.com/RafaelAugustScherer/trybe-rank): MERN e Deploy no Heroku

- 🟨 [TodoListChallenge](): Desafio Técnico Fictício da Trybe

- 🗃️ [SharedSpace](https://gitlab.com/rafaelaugustscherer/sharedspace): Manipulação de Arquivos em JS